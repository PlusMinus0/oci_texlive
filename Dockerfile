FROM debian:buster-slim

RUN apt-get update -yqq && apt-get install -yqq texlive-latex-recommended texlive-latex-extra latexmk biber texlive-luatex
